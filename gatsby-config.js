module.exports = {
  // pathPrefix: `/gatsby/`,
  siteMetadata: {
    title: `Balu Balu`,
    description: `A celebration of the best Bernese Mountain Dog to ever have walked this earth.`,
    author: `@tballast`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-plugin-image`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#ff8906`,
        theme_color: `#ff8906`,
        display: `minimal-ui`,
        icon: `src/images/bernie.png`, // This path is relative to the root of the site.
      },
    },
    `gatsby-plugin-typescript`,

    // Colours
    // Background: #0f0e17
    // Headline: #fffffe
    // Paragraph: #a7a9be
    // Button: #ff8906
    // Button Text: #fffffe

    // Stroke: black
    // Main: #fffffe
    // Highlight: #ff8906
    // Secondary: #f25f4c
    // Tertiary: #e53170

    // https://www.happyhues.co/palettes/13

    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
