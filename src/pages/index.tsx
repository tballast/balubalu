import { graphql } from 'gatsby'
import * as React from 'react'
import { IGatsbyImageData } from 'gatsby-plugin-image'

import Layout from '../components/layout'
import SEO from '../components/seo'
import Gallery from '@browniebroke/gatsby-image-gallery'
// import Gallery from '../../../gatsby-image-gallery/src'

import './styles.css'

// TODO build your own with this
// https://www.npmjs.com/package/simple-react-lightbox
// TODO add an exif reader to display some basic info with the images.
// TODO use the google photos api to fetch the latest pics of the bub

interface ImageSharpEdge {
  node: {
    childImageSharp: {
      thumb: IGatsbyImageData
      full: IGatsbyImageData
      meta: {
        originalName: string
      }
    }
  }
}

interface PageProps {
  data: {
    images: {
      edges: ImageSharpEdge[]
    }
  }
}

const IndexPage: React.FC<PageProps> = ({ data }) => {
  const images = data.images.edges
    .map(({ node }) => {
      console.log('Node', node)
      if (node.childImageSharp) {
        return {
          ...node.childImageSharp,
          // Use original name as caption.
          // The `originalName` is queried in a nested field,
          // but the `Gallery` component expects `caption` at the top level.
          caption: node.childImageSharp.meta.originalName,
        }
      } else {
        console.count()
        return null
      }
    })
    .filter(i => Boolean(i))

  // Override some of Lightbox options to localise labels in French
  const lightboxOptions = {
    imageLoadErrorMessage: 'Fehler beim Laden',
    nextLabel: 'Nächstes Bild',
    prevLabel: 'Letztes Bild',
    zoomInLabel: 'Größer',
    zoomOutLabel: 'Kleiner',
    closeLabel: 'Schließen',
  }

  //Add callback to Lightbox onCloseRequest
  const onClose = () => {
    console.log('Lightbox was closed')
  }

  return (
    <Layout>
      <SEO title='Gallery' />
      <h1>The King of Lilse</h1>

      <Gallery
        images={images}
        lightboxOptions={lightboxOptions}
        onClose={onClose}
      />
    </Layout>
  )
}

export const pageQuery = graphql`
  query ImagesForGallery {
    images: allFile(
      filter: { relativeDirectory: { eq: "balu" } }
      sort: { fields: name }
    ) {
      edges {
        node {
          childImageSharp {
            thumb: gatsbyImageData(
              width: 270
              height: 270
              placeholder: BLURRED
            )
            full: gatsbyImageData(layout: FULL_WIDTH)
            meta: fixed {
              originalName
            }
          }
        }
      }
    }
  }
`

export default IndexPage
