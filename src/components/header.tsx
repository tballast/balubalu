import { Link } from 'gatsby'
import * as PropTypes from 'prop-types'
import * as React from 'react'

interface PropTypes {
  siteTitle: string
}

const Header: React.FC<PropTypes> = ({ siteTitle = `` }) => (
  <header
    style={{
      // background: `linear-gradient(295deg, #814b23 0%, #945424 100%)`,
      backgroundColor: '#ff8906',
      color: '#fffffe',
      marginBottom: `1.45rem`,
    }}
  >
    <div
      style={{
        margin: `0 auto`,
        maxWidth: 960,
        padding: `1.45rem 1.0875rem`,
      }}
    >
      <h1 style={{ margin: 0 }}>
        <Link
          to='/'
          style={{
            color: `inherit`,
            textDecoration: `none`,
          }}
        >
          {siteTitle}
        </Link>
      </h1>
    </div>
  </header>
)

export default Header
